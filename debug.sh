H187=10.21.0.187
H188=10.21.0.188
H202=10.21.2.202
HICT=159.226.39.91
HBRI=159.226.39.95
BRIP=9527
ARMP=9002
USR=root
DBDIR=/home/lyb
CPDST=$USR@$H187:$DBDIR/
L187=187.log
ODPDIR=/root/open-estuary-rc1/packages/odp/odp1.7
ARM_PREFIX="/root/open-estuary-rc1/toolchain/gcc-linaro-aarch64-linux-gnu-4.9-2014.09_linux/bin/aarch64-linux-gnu"

ARG_TGT=
ARG_CP=0
ARG_ICT=0
RUN_SERVER=
RUN_CLIENT=
RUN_COMPILE=
TERM_TGT=
ARG_INIT=
ARG_PUSH=
ARG_RELOAD=
ARG_REPORT=

for i in "$@"
do
case $i in
    -l=*|--login=*)
    ARG_TGT="${i#*=}"
    shift # past argument=value
    ;;
    -p=*|--push=*)
    ARG_PUSH="${i#*=}"
    shift # past argument=value
    ;;
    -i=*|--init=*)
    ARG_INIT="${i#*=}"
    shift # past argument with no value
    ;;
    -r=*|--reload=*)
    ARG_RELOAD="${i#*=}"
    shift # past argument with no value
    ;;
    -s=*|--server=*)
    RUN_SERVER="${i#*=}"
    shift # past argument with no value
    ;;
    -c=*|--client=*)
    RUN_CLIENT="${i#*=}"
    shift # past argument with no value
    ;;
    -C=*|--compile=*)
    RUN_COMPILE="${i#*=}"
    shift # past argument with no value
    ;;
    -t=*|--term=*)
    TERM_TGT="${i#*=}"
    shift # past argument with no value
    ;;
     -R=*|--report=*)
    ARG_REPORT="${i#*=}"
    shift # past argument with no value
    ;;
    *)
            # unknown option
    ;;
esac
done

function run_command() {
    if [ $# -eq 3 ]; then
	ssh -l $USR $1 "ssh -l $USR $2 '$3'"
    elif [ $# -eq 2 ]; then
	ssh -l $USR $1 "$2"
    else
	echo "$1"
    fi
}

function login() {
    if [ $1 == '202' ]; then
	ssh -p 9527 $USR@$HBRI
    elif [ $1 == '187' ]; then
	ssh $USR@$H187
    elif [ $1 == '188' ]; then
	ssh $USR@$H188
    elif [ $1 == 'arm' ]; then
	#ssh -p $ARMP $USR@$H202
	run_command $HICT "ssh -p $ARMP $USR@$H202"
    elif [ $1 == 'ict' ]; then
	ssh $USR@$HICT
    fi
}

function report() {
    REPORT=log.report.txt
    HARM=192.168.1.2
    COMMANDS="cd $DBDIR && rm $REPORT && bash report.sh $1"
    
    ssh -l $USR -p $BRIP $HBRI "ssh -l $USR $HARM '$COMMANDS'"
    ssh -l $USR -p $BRIP $HBRI "scp $USR@$HARM:$DBDIR/$REPORT $DBDIR/"
    scp -P $BRIP $USR@$HBRI:$DBDIR/$REPORT ./
}

function copy_add() {
    [[ $# -eq 2 ]] || echo "invalid arguments for copy_add" && exit

    SRC=$1
    DST=$2
    [[ -d $DST ]] && DST=$2/`basename $1`

    cp $SRC $DST
    git add $DST
}

function push() {
    [[ -z $1 ]] && echo "no git specified!" && exit

    SUFFIX="h c in ac"
    CDIR=changed-files
    CPSH=$CDIR/copy.sh
    PREV=9d7dfd
    cd mtcp && git checkout $1 && CFILES=`git diff HEAD $PREV --stat` && cd ..

    if [ -f $CPSH ]; then
	OFILES=`cat $CPSH | grep ^cp`
	for i in $OFILES; do
	    [[ $i != 'cp' ]] && [[ $i != ../mtcp/* ]] && [ -f $CDIR/$i ] && git rm $CDIR/$i
	done
	rm $CPSH
    fi

    echo "#!/bin/bash" > $CPSH && echo "" >> $CPSH
    echo "# !please git checkout $PREV on mtcp first! #" >> $CPSH && echo "" >> $CPSH
    echo "# just run this script to copy required files from current dir to ../mtcp/" >> $CPSH
    
    for i in $CFILES; do
	for j in $SUFFIX; do
	    COND=`echo $i | grep ".$j$"`
	    if [ ! -z $COND ]; then
		FILE=`basename $i`
		DIR=`dirname $i`
		[[ -f $CDIR/$FILE ]] && FILE=$FILE.${DIR##*/}
		cp mtcp/$i $CDIR/$FILE
		git add $CDIR/$FILE
		echo "command for: copy $FILE to ../mtcp/$i"
		echo "cp $FILE ../mtcp/$i" >> $CPSH
	    fi
	done
    done

    git add $CPSH
    git commit -m "sync with $1"
    git push origin master:master
}

function copy_to() {
    [[ $# -eq 0 ]] && echo "no object to copy!" && exit

    CPOBJ=$1
    CPTGT=$2
    CPBRI=$3
    
    if [ -d $CPOBJ ]; then
	SAFERM="[[ -d $CPOBJ ]] && rm -rf $CPOBJ"
	SAFESCP="scp -r $CPOBJ"
    else
	SAFERM="[[ -f $CPOBJ ]] && rm $CPOBJ"
	SAFESCP="scp $CPOBJ"
    fi
    
    if [ -z $CPBRI ]; then
	run_command $CPTGT "cd $DBDIR && $SAFERM"
	$SAFESCP $USR@$CPTGT:$DBDIR/
    else
	run_command $CPBRI "cd $DBDIR && $SAFERM"
	$SAFESCP $USR@$CPBRI:$DBDIR/
	run_command $CPBRI $CPTGT "cd $DBDIR && $SAFERM"
	run_command $CPBRI "cd $DBDIR && $SAFESCP $USR@$CPTGT:$DBDIR/"
    fi
	
}

function copy_to_via() {
    RDIR=/home/lyb

    SFILE=$1
    DSTIP=$2
    BRIIP=$3
    BRIDIR=$4
    DSTPORT=
    CMDTP=
    CPTP=

    arr=(${DSTIP//:/ })
    num=${#arr[@]}
    if [ $num -eq 2 ]; then
	DSTIP=${arr[0]}
	DSTPORT=${arr[1]}
	CMDTP=-p
	CPTP=-P
	
    fi

    CPDIR=`dirname $SFILE`
    CPTGT=`basename $SFILE`
    [[ -d $SFILE ]] && OPTR=-r || OPTR=

    [[ $CPDIR =~ .*debug/$BRIDIR.* ]] && CPDIR=${CPDIR##*"$BRIDIR"}
    
    [[ $CPDIR == '.' ]] && TGTDIR=$RDIR/$BRIDIR || TGTDIR=$RDIR/$BRIDIR/$CPDIR
    [[ $CPDIR == ' ' ]] && TGTDIR=$RDIR/$BRIDIR || TGTDIR=$RDIR/$BRIDIR/$CPDIR
    run_command $BRIIP "[[ -d $TGTDIR ]] || mkdir -p $TGTDIR"
    echo "copy $SFILE to $USR@$BRIIP:$TGTDIR/"
    scp $OPTR $SFILE $USR@$BRIIP:$TGTDIR/ > /dev/null

    SFILE=$TGTDIR/$CPTGT
    [[ $CPDIR == '.' ]] && TGTDIR=$RDIR || TGTDIR=$RDIR/$CPDIR
    run_command $BRIIP "$CMDTP $DSTPORT $DSTIP" "[[ -d $TGTDIR ]] || mkdir -p $TGTDIR"
    echo "copy $BRIIP:$SFILE to $USR@$DSTIP:$TGTDIR/ -P=$DSTPORT"
    run_command $BRIIP "scp $CPTP $DSTPORT $OPTR $SFILE $USR@$DSTIP:$TGTDIR/"
}

function init() {
    arr=(${ARG_INIT//:/ })
    if [ ${#arr[@]} -eq 2 ]; then
	SGIT=${arr[0]}
	CGIT=${arr[1]}
    else
	SGIT=$ARG_INIT
	CGIT=$ARG_INIT
    fi

    [[ -z $SGIT ]] && echo "[ERROR]: server git is empty" && exit
    [[ -z $CGIT ]] && echo "[ERROR]: client git is empty" && exit

    # init 187 for compile server
    echo "checkout on $SGIT for server"
    #cd mtcp && git checkout $SGIT && cd ..
    #for i in `ls mtcp | grep -v ^dpdk`; do
    #	copy_to_via mtcp/$i $H187 $HICT server
    #done

    MTCPDIR=$DBDIR/mtcp
    run_command $HICT $H187 "cd $MTCPDIR && ./configure --with-odp-lib=$ODPDIR"
    run_command $HICT $H187 "cd $MTCPDIR && make clean && make"

    #################################################################
    
    # init 188 for compile client
    echo "checkout on $CGIT for client"
    #cd mtcp && git checkout $CGIT && cd ..
    #for i in `ls mtcp | grep -v ^dpdk`; do
    #	copy_to_via mtcp/$i $H188 $HICT client
    #done

    # compile dpdk on client
    DPDKDIR=$DBDIR/mtcp/`ls mtcp | grep ^dpdk-`
    GCC=x86_64-native-linuxapp-gcc
    #run_command $HICT $H188 "cd $DPDKDIR && make config T=$GCC O=$GCC"
    #run_command $HICT $H188 "cd $DPDKDIR && make install T=$GCC EXTRA_CFLAGS='-fPIC'"
    
    LIBDIR=$DPDKDIR/../dpdk
    #run_command $HICT $H188 "[[ -d $LIBDIR ]] && mkdir -p $LIBDIR"
    #run_command $HICT $H188 "cd $LIBDIR && ln -s $DPDKDIR/$GCC/lib lib"
    #run_command $HICT $H188 "cd $LIBDIR && ln -s $DPDKDIR/$GCC/include include"

    # compile mtcp on client
    #run_command $HICT $H188 "cd $DBDIR/mtcp && ./configure --with-dpdk-lib=$LIBDIR"
    #run_command $HICT $H188 "rm /usr/lib/libmtcp.a"
    #run_command $HICT $H188 "cd $DBDIR/mtcp && make clean && make"

    ABDIR=$DBDIR/mtcp/apps/apache_benchmark
    MTCPDIR=$DBDIR/mtcp/mtcp
    DPDKDIR=$DBDIR/mtcp/dpdk
    run_command $HICT $H188 "cd $ABDIR && ./configure CFLAGS=\"-g -O3\" --with-libmtcp=$MTCPDIR --with-libdpdk=$DPDKDIR"
    run_command $HICT $H188 "cd $ABDIR && make"
}

function reload_on_host() {
    [[ $# -ne 3 ]] && echo "invalid formate to reload" && exit
    SUFFIX="h c in ac"
    CFILES=
    PREV=origin/master
    cd mtcp && PREV=`git rev-parse HEAD` && git checkout $1 && CFILES=`git diff HEAD $PREV --stat` && cd ..
    for i in $CFILES; do
	for j in $SUFFIX; do
	    COND=`echo $i | grep ".$j$"`
	    if [ ! -z $COND ]; then
		copy_to_via mtcp/$i $2 $HICT $3
	    fi
	done
    done
}

function reload() {
    arr=(${ARG_RELOAD//:/ })
    if [ ${#arr[@]} -eq 2 ]; then
	SGIT=${arr[0]}
	CGIT=${arr[1]}
    elif [ ! -z `echo $ARG_RELOAD | grep ^:` ]; then
	CGIT=${ARG_RELOAD#:}
    elif [ ! -z `echo $ARG_RELOAD | grep :$` ]; then
	SGIT=${ARG_RELOAD%:}
    fi

    [[ ! -z $SGIT ]] && reload_on_host $SGIT $H187 server
    [[ ! -z $CGIT ]] && reload_on_host $CGIT $H188 client
}

function compile_server() {
    for i in "$@"
    do
	copy_to_via $i $H187 $HICT server
    done

    MTCPDIR=$DBDIR/mtcp
    SRCDIR=$MTCPDIR/mtcp/src
    APPDIR=$MTCPDIR/apps
    EXPDIR=$APPDIR/example
    HTTPDIR=`ls mtcp/apps | grep lighttpd`

    C_MTCP=1 # for debug else 1
    C_EPS=0
    C_HTTP=0
    CFG=0

    HTTPEXE=lighttpd
    CFG_EXE=
    
    if [ $RUN_COMPILE = 's' ]; then
	C_EPS=1
	C_HTTP=1
    elif [ $RUN_COMPILE = 'eps' ]; then
	C_EPS=1
    elif [ $RUN_COMPILE = 'ht' ]; then
	C_HTTP=1
    elif [ $RUN_COMPILE = 'htm' ]; then
	C_HTTP=1
	CFG=1
	CFG_EXE=m-$HTTPEXE
	EXTRA_LIB_OPT="--with-libmtcp=$MTCPDIR/mtcp --with-libodp=$ODPDIR"
    elif [ $RUN_COMPILE = 'hto' ]; then
	C_HTTP=1
	CFG=1
	CFG_EXE=o-$HTTPEXE
	EXTRA_LIB_OPT="--enable-multithreading"
    else
	C_MTCP=0
    fi

    [[ -z $EXPDIR ]] && C_EPS=0
    [[ -z $HTTPDIR ]] && C_HTTP=0

    HTTPDIR=$APPDIR/$HTTPDIR
    
    if [ $C_MTCP -eq 1 ]; then
	echo "[LYB] preparing mtcp!"
	run_command $HICT $H187 "cd $SRCDIR && make"
    fi

    if [ $C_EPS -eq 1 ]; then
	echo "[LYB] preparing epserver!"
	run_command $HICT $H187 "cd $EXPDIR && rm epserver && make"
	run_command $HICT $H187 "cd $EXPDIR && scp -P $ARMP epserver $USR@$H202:$DBDIR/"
    fi

    if [ $C_HTTP -eq 1 ]; then
	echo "[LYB] preparing lighttpd!"
	PCREDIR=/home/st/mtcp-odp/mtcp/apps/apache_benchmark/srclib/pcre
	NUMADIR=/home/st/numa-arm/numactl-2.0.8~rc3
	[[ -z $CFG_EXE ]] && CFG_EXE=`run_command $HICT $H187 "cd $HTTPDIR/src && ls *-$HTTPEXE"`
	if [ $CFG -eq 1 ]; then
	    run_command $HICT $H187 "cd $HTTPDIR && ./configure --host=arm-aarch64-linux $EXTRA_LIB_OPT --without-zlib --without-bzip2 CC=${ARM_PREFIX}-gcc LD=${ARM_PREFIX}-ld AR=${ARM_PREFIX}-ar CPP=\"${ARM_PREFIX}-gcc -E\" CFLAGS=\"-g -O3\" LDFLAGS=\"-L/usr/lib -L/home/hepeng/arm-lib/lib -L$PCREDIR\" CPPFLAGS=\"-I$PCREDIR -I$NUMADIR\" && make clean"
	    run_command $HICT $H187 "cd $HTTPDIR/src && rm m-$HTTPEXE o-$HTTPEXE" 
	fi

	run_command $HICT $H187 "cd $HTTPDIR && make"

	[[ -z $CFG_EXE ]] && echo "please configure first!" && exit

	echo "copy $CFG_EXE to $DBDIR/ at $H202:$ARMP"
	run_command $HICT $H187 "cd $HTTPDIR/src && mv $HTTPEXE $CFG_EXE"
	run_command $HICT $H187 "scp -P $ARMP $HTTPDIR/src/$CFG_EXE $USR@$H202:$DBDIR/"
    fi
}

function compile_client() {    
    for i in "$@"
    do
	copy_to_via $i $H188 $HICT client
    done

    C_MTCP=1 # for debug else 1
    C_GET=0
    C_AB=0
    CFG=0
    ABEXE=ab
    CFG_EXE=

    MTCPDIR=$DBDIR/mtcp
    SRCDIR=$MTCPDIR/mtcp/src
    EXPDIR=$MTCPDIR/apps/example
    ABDIR=$MTCPDIR/apps/apache_benchmark
    ABSUP=$ABDIR/support
    PCRE=$ABDIR/srclib/pcre

    if [ $RUN_COMPILE = 'c' ]; then
	C_GET=1
	C_AB=1
    elif [ $RUN_COMPILE = 'get' ]; then
	C_GET=1
    elif [ $RUN_COMPILE = 'ab' ]; then
	C_AB=1
    elif [ $RUN_COMPILE = 'abm' ]; then
	C_AB=1
	CFG=1
	CFG_EXE=m-$ABEXE
	EXTRA_LIB_OPT="--with-libmtcp=$MTCPDIR/mtcp --with-libdpdk=$MTCPDIR/dpdk"
    elif [ $RUN_COMPILE = 'abo' ]; then
	C_AB=1
	CFG=1
	CFG_EXE=o-$ABEXE
	EXTRA_LIB_OPT="--enable-multithreading"
    else
	C_MTCP=0
    fi

    [[ -z $EXPDIR ]] && C_GET=0
    [[ -z $ABDIR ]] && C_AB=0

    if [ $C_MTCP -eq 1 ]; then
	echo "[LYB] preparing mtcp!"
	run_command $HICT $H188 "cd $SRCDIR && make"
	
    fi

    if [ $C_GET -eq 1 ]; then
	echo "[LYB] preparing epwget!"
	run_command $HICT $H188 "cd $EXPDIR && rm epwget && make"
	run_command $HICT $H188 "cd $EXPDIR && cp epwget $DBDIR/"
    fi

    if [ $C_AB -eq 1 ]; then
	echo "[LYB] preparing ab!"
	[[ -z $CFG_EXE ]] && CFG_EXE=`run_command $HICT $H188 "cd $ABSUP && ls *-$ABEXE"`
	if [ $CFG -eq 1 ]; then
	    run_command $HICT $H188 "cd $ABDIR && ./configure $EXTRA_LIB_OPT CFLAGS=\"-g -O3\" && make clean"
	    run_command $HICT $H188 "cd $ABSUP && rm m-$ABEXE o-$ABEXE" 
	fi

	run_command $HICT $H188 "cd $ABDIR && make"

	[[ -z $CFG_EXE ]] && echo "please configure first!" && exit

	echo "copy $CFG_EXE to $DBDIR/ at $H188"
	run_command $HICT $H188 "cd $ABSUP && mv $ABEXE $CFG_EXE"
	run_command $HICT $H188 "cd $DBDIR && rm -r .libs && cp $ABSUP/$CFG_EXE $DBDIR/"
    fi
    
}

function run_compile() {
    SERVER="s eps htm hto ht"
    CLIENT="c get abm abo ab"

    VALID=0
    for i in $SERVER; do
	[[ $i == $RUN_COMPILE ]] && VALID=1 && compile_server $@ && exit
    done

    for i in $CLIENT; do
	[[ $i == $RUN_COMPILE ]] && VALID=1 && compile_client $@ && exit
    done

    if [ $VALID -eq 0 ]; then
	echo "invalid compile option $RUN_COMPILE, valid options are:"
	echo "for server: $SERVER"
	echo "for client: $CLIENT"
    fi
}

function run_server() {
    for i in "$@"
    do
	copy_to_via $i $H202:$ARMP $HICT server
    done

    if [ $RUN_SERVER == 'eps' ]; then
	COMMANDS="./epserver -p $DBDIR/www -f epserver.conf -N 1"
	echo "run epserver on $H202:$ARMP @ $COMMANDS"
    elif [ $RUN_SERVER == 'htm' ]; then
	COMMANDS="cp config/mtcp.conf $DBDIR/ && ./m-lighttpd -D -f config/m-lighttpd.conf -m config/lib -n 1"
	echo "run m-lighttpd on $H202:$ARMP @ $COMMANDS"
    elif [ $RUN_SERVER == 'hto' ]; then
	COMMANDS="./o-lighttpd -D -f config/lighttpd.orig.conf -m config/lib -n 1"
	echo "run o-lighttpd on $H202:$ARMP @ $COMMANDS"
    else
	echo "invalid run_server option $RUN_SERVER (eps or htm or hto)" && exit
    fi

    run_command $HICT "-p $ARMP $H202" "cd $DBDIR && $COMMANDS"
}

function run_client() {
    for i in "$@"
    do
	copy_to_via $i $H188 $HICT client
    done

    entry=`cat changed-files/debug/client/epwget.conf | grep ^port`
    entry=${entry#*port = } && SERVERIP=10.0.${entry##*dpdk}.${ARMP##*900}
    echo "test for SERVER at $SERVERIP"
    
    FILE=4k.test

    if [ $RUN_CLIENT == 'abm' ]; then
	COMMANDS="cd $DBDIR && ./m-ab -N 8 -c 100 -n 1000000 -k 1 $SERVERIP/$FILE 2>&1"
    elif [ $RUN_CLIENT == 'abo' ]; then
	COMMANDS="cd $DBDIR && ./o-ab -N 8 -c 10000 -n 200000 -k 16 $SERVERIP/$FILE 2>&1"
    elif [ $RUN_CLIENT == 'get' ]; then
	COMMANDS="cd $DBDIR && ./epwget $SERVERIP/$FILE 1 -N 1 -o $FILE 2>& 1 && cat $FILE.0"
    else
	echo "invalid run_server option $RUN_CLIENT (get or abm or abo)"
    fi

    run_command $HICT $H188 "$COMMANDS"
}

function stop_server() {
    EXE="epserver m-lighttpd o-lighttpd"
    run_command $HICT "-p $ARMP $H202" "cat $DBDIR/www/lighttpd.pid | kill"
    for p in $EXE; do
	run_command $HICT "-p $ARMP $H202" "killall $@ $p"
    done
}

function stop_client() {
    EXE="epwget lt-ab"
    for p in $EXE; do
	run_command $HICT $H188 "killall $@ $p"
    done
}

[[ ! -z $ARG_TGT ]]    && login $ARG_TGT && exit
[[ ! -z $ARG_REPORT ]] && report $ARG_REPORT && exit
[[ ! -z $ARG_PUSH ]]   && push $ARG_PUSH && exit
[[ ! -z $ARG_INIT ]]   && init $H187 && exit
[[ ! -z $ARG_RELOAD ]] && reload && exit

[[ ! -z $RUN_COMPILE ]]  && run_compile $@ && exit
[[ ! -z $RUN_SERVER ]]   && run_server $@ && exit
[[ ! -z $RUN_CLIENT ]]   && run_client $@ && exit
[[ $TERM_TGT == 's' ]]   && stop_server $@ && exit
[[ $TERM_TGT == 'c' ]]   && stop_client $@ && exit
if [[ $TERM_TGT == 'a' ]]; then
    stop_server $@
    stop_client $@
    exit
fi

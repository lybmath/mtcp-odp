#! /bin/bash
ROOTDIR=$(pwd)
ODPDIR=$ROOTDIR/odp-dpdk
MTCPDIR=mtcp
CFDIR=changed-files
LIBPATH=$ODPDIR/lib
ODPLIB=$LIBPATH/libodp-dpdk.la
INSPATH=/usr/local/lib
INSLIB=$INSPATH/libodp-dpdk.so
CP=cp

TGT=x86
RUN_ON_SERVER=0
RUN_ON_CLIENT=0
DEBUG=0
ROOLBACK=0

for i in "$@"
do
case $i in
    -t=*|--target=*)
    TGT="${i#*=}"
    shift # past argument=value
    ;;
    -s|--server)
    RUN_ON_SERVER=1
    shift # past argument with no value
    ;;
    -c|--client)
    RUN_ON_CLIENT=1
    shift # past argument with no value
    ;;
    -d|--debug)
    DEBUG=1
    shift # past argument with no value
    ;;
    -r|--roolback)
    ROOLBACK=1
    shift # past argument with no value
    ;;
    *)
            # unknown option
    ;;
esac
done

function debug_roolback() {
    SRCDIR=$MTCPDIR/mtcp/src
    EXPDIR=$MTCPDIR/apps/exmaple
    DBGDIR=$CFDIR/debug
}

if [ $ROOLBACK -eq 1 ]; then
    debug_roolback
    exit
fi
    
function prepare_arm() {
    echo "this is for arm...."
    ODPDIR=/root/open-estuary-rc1/packages/odp/odp1.7
    INSPATH=/usr/lib:/home/hepeng/arm-lib/lib
    $CP $CFDIR/$TGT/Makefile.in.util $MTCPDIR/util/Makefile.in
    $CP $CFDIR/$TGT/ps.h             $MTCPDIR/io_engine/include/
}

function prepare_x86() {
    echo "this is for x86...."
    # check whether libodp-dpdk is ready
    if [[ -f $ODPLIB ]]; then
	[[ -f $INSLIB ]] || sudo libtool --mode=install install -c $ODPLIB $INSPATH
    else
	echo "please compile odp-dpdk first"
	exit
    fi
}

[[ $TGT == "arm" ]] && prepare_arm || prepare_x86

# ensure /usr/local/lib is in the lib search path
[[ $LD_LIBRARY_PATH == *$INSPATH* ]] || export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSPATH

# copy changed files to traget places
#----------------------------------------------------------------#
$CP $CFDIR/configure.ac          $MTCPDIR/
$CP $CFDIR/io_module.c           $MTCPDIR/mtcp/src/
$CP $CFDIR/$TGT/core.c           $MTCPDIR/mtcp/src/
$CP $CFDIR/$TGT/odp_module.c     $MTCPDIR/mtcp/src
$CP $CFDIR/$TGT/io_module.h      $MTCPDIR/mtcp/src/include/
$CP $CFDIR/$TGT/Makefile.in.src  $MTCPDIR/mtcp/src/Makefile.in
$CP $CFDIR/$TGT/Makefile.in.exp  $MTCPDIR/apps/example/Makefile.in
#----------------------------------------------------------------#

MTCPCFG=$MTCPDIR/configure
MTCPLIB=$MTCPDIR/mtcp/lib/libmtcp.a

# configure mtcp
function configure_mtcp() {
    cd $MTCPDIR
    autoconf
    cd ..
}

# compile mtcp
function compile_mtcp() {
    cd $MTCPDIR
    if [[ $# -eq 0 ]]; then
	echo "just make"
	make clean && make
    else
	echo "configure and make"
	./configure --with-odp-lib=$1
	make clean && make
    fi
    cd ..
}

function debug_compile() {
    cd $MTCPDIR
    if [ ! -z $1 ]; then
	./configure --with-odp-lib=$1
	make clean && make
    else
	cd mtcp/src && make && cd ../../
	cd util && make && cd ../
	cd apps/example && make clean && make && cd ../../
	make clean && make
    fi
    cd ..
}

if [ $DEBUG -eq 1 ]; then
    [[ -d bin ]] || mkdir -p bin
    debug_compile #$ODPDIR
    EXPDIR=$MTCPDIR/apps/example
    cp $EXPDIR/epserver bin/
    cp $EXPDIR/epwget bin/
    cp $EXPDIR/epserver.conf bin/
    cp $EXPDIR/epwget.conf bin/
else
    [[ -f $MTCPCFG ]] || configure_mtcp
    [[ -f $MTCPLIB ]] && compile_mtcp || compile_mtcp $ODPDIR
fi
#----------------------------------------------------------------#

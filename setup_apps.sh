#!/bin/bash
#
# author: Tong Shen
# create: 9/21/2016
# update: 9/26/2016
# version: 2.0
#

ARM_PREFIX="/root/open-estuary-rc1/toolchain/gcc-linaro-aarch64-linux-gnu-4.9-2014.09_linux/bin/aarch64-linux-gnu"
ROOT_DIR="/home/st/mtcp-odp/mtcp"
MTCP_DIR="${ROOT_DIR}/mtcp"
APPS_DIR="${ROOT_DIR}/apps"
TARGET_HOST="arm-aarch64-linux"
CFILES_DIR="/home/st/mtcp-odp/changed-files"

#-----------#
# setup apr #
#-----------#
function setup_apr() {
	cd srclib/apr
	./configure \
		--prefix=${APPS_DIR}/${APP_NAME}/srclib/apr/apr \
		--host=${TARGET_HOST} \
		CC=${ARM_PREFIX}-gcc LD=${ARM_PREFIX}-ld AR=${ARM_PREFIX}-ar CPP="${ARM_PREFIX}-gcc -E" \
		CFLAGS="-g -O3" \
		ac_cv_file__dev_zero=yes \
		ac_cv_func_setpgrp_void=yes \
		apr_cv_tcp_nodelay_with_cork=yes
	make clean && make && make install
	cd ../..
}

#----------------#
# setup apr-util #
#----------------#
function setup_apr_util() {
	cd srclib/apr-util
	./configure \
		--prefix=${APPS_DIR}/${APP_NAME}/srclib/apr-util/apr-util \
		--host=${TARGET_HOST} \
		--with-apr=${APPS_DIR}/${APP_NAME}/srclib/apr/apr \
		CC=${ARM_PREFIX}-gcc LD=${ARM_PREFIX}-ld AR=${ARM_PREFIX}-ar CPP="${ARM_PREFIX}-gcc -E" \
		CFLAGS="-g -O3"
	make clean && make && make install
	cd ../..
}

#------------------------#
# setup apache benchmark #
#------------------------#
function setup_ab() {
	APP_NAME="apache_benchmark"
	cd ${APP_NAME}
	cp -f ${CFILES_DIR}/apps/ab/apr_want.h 			srclib/apr/include/
	cp -f ${CFILES_DIR}/apps/ab/Makefile.in.pcre 	srclib/pcre/Makefile.in
	cp -f ${CFILES_DIR}/apps/ab/Makefile.in.support support/Makefile.in
	setup_apr 		# call subfunction
	setup_apr_util 	# call subfunction
	./configure \
		--host=${TARGET_HOST} \
		--with-libmtcp=${MTCP_DIR}/lib \
		--with-apr=${APPS_DIR}/${APP_NAME}/srclib/apr/apr \
		--with-apr-util=${APPS_DIR}/${APP_NAME}/srclib/apr-util/apr-util \
		CC=${ARM_PREFIX}-gcc LD=${ARM_PREFIX}-ld AR=${ARM_PREFIX}-ar CPP="${ARM_PREFIX}-gcc -E" \
		CFLAGS="-g -O3" \
		LDFLAGS="-L/usr/lib -L/home/st/mtcp-odp/mtcp/mtcp/lib" \
		CPPFLAGS="-I/home/st/mtcp-odp/mtcp/mtcp/include" \
		ap_cv_void_ptr_lt_long=no
	make clean && make
	cd ..
}

#----------------#
# setup lighttpd #
#----------------#
function setup_lighttpd() {
	APP_NAME="lighttpd-1.4.32"
	cd ${APP_NAME}
	cp -f ${CFILES_DIR}/apps/lighttpd/network_backends.h 	src/
	cp -f ${CFILES_DIR}/apps/lighttpd/Makefile.am.src 		src/Makefile.am
	ln -s /usr/share/automake-1.14/test-driver test-driver
	./configure \
		--prefix=${APPS_DIR}/${APP_NAME}/lighttpd \
		--host=${TARGET_HOST}  \
		--with-libmtcp=${MTCP_DIR} \
		--without-zlib \
		--without-bzip2 \
		CC=${ARM_PREFIX}-gcc LD=${ARM_PREFIX}-ld AR=${ARM_PREFIX}-ar CPP="${ARM_PREFIX}-gcc -E" \
		CFLAGS="-g -O3" \
		LDFLAGS="-L/usr/lib -L/home/st/mtcp-odp/mtcp/apps/apache_benchmark/srclib/pcre" \
		CPPFLAGS="-I/home/st/mtcp-odp/mtcp/mtcp/include \
			-I/home/st/mtcp-odp/mtcp/mtcp/src/include \
			-I/home/st/mtcp-odp/mtcp/apps/apache_benchmark/srclib/pcre \
			-I/home/st/numa-arm/numactl-2.0.8~rc3"
	make clean && make && make install
	cd ..
}

cd ${APPS_DIR}
setup_ab
setup_lighttpd

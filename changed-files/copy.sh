#!/bin/bash

# !please git checkout 9d7dfd on mtcp first! #

# just run this script to copy required files from current dir to ../mtcp/
cp Makefile.in ../mtcp/apps/example/Makefile.in
cp epserver.c ../mtcp/apps/example/epserver.c
cp configure.ac ../mtcp/configure.ac
cp configure.ac.. ../mtcp/configure.ac
cp ps.h ../mtcp/io_engine/include/ps.h
cp Makefile.in.src ../mtcp/mtcp/src/Makefile.in
cp api.c ../mtcp/mtcp/src/api.c
cp arp.c ../mtcp/mtcp/src/arp.c
cp core.c ../mtcp/mtcp/src/core.c
cp dpdk_module.c ../mtcp/mtcp/src/dpdk_module.c
cp eth_out.c ../mtcp/mtcp/src/eth_out.c
cp io_module.h ../mtcp/mtcp/src/include/io_module.h
cp mtcp.h ../mtcp/mtcp/src/include/mtcp.h
cp io_module.c ../mtcp/mtcp/src/io_module.c
cp ip_in.c ../mtcp/mtcp/src/ip_in.c
cp odp_module.c ../mtcp/mtcp/src/odp_module.c
cp tcp_out.c ../mtcp/mtcp/src/tcp_out.c
cp tcp_stream.c ../mtcp/mtcp/src/tcp_stream.c
cp Makefile.in.util ../mtcp/util/Makefile.in
